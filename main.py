
import os, sys
from pathlib import Path
# import urllib
from urllib.request import urlretrieve

module_name="config_example"
commands = ["config", "httpcheck", "help", "update", "list"]
configfile = '~/.hmgcli.d/'+ module_name+ '.conf'
configkeys = ["url", "user", "password"]
cfg = {}


def execute(command,params):
    # print ("Command: ", command)
    if command == "config":
        if len(params) == 0:
            print ("Missing parameter: show|create")
            sys.exit()
        if params == "create":
            config_example_create()
        if params[0] == "show":
            config_example_show()
    elif command == "httpcheck":
        config_example_httpcheck()
    elif command == "update":
        config_example_update()
    elif command == "list":
        config_example_list()
    elif command == "help":
        config_example_help()

def config_example_list():
    filepath = 'packages.data'
    with open(filepath) as fp:
        line = fp.readline()
        cnt = 1
        while line:
            print("{}: {}".format(cnt, line.strip()))
            line = fp.readline()
            cnt += 1

def config_example_update():
    # urllib.urlretrieve ("http://localhost/packages.data", "packages.data")
    print ("Download package list ...")
    urlretrieve("http://localhost/packages.data","packages.data")


def config_example_help():
    print ("Example Modul als Beispiel.")
    print ("Konfiguration für das Module anlegen, anzeigen und im Modul benutzen.\n")
    print ("Konfiguration anlegen:")
    print ("  hmgcli config_exmaple create")
    print ("Es werden alle Config Keys die in configkeys abgefragt.")
    print ("Config Section ist der Modulename in der INI Datei.\n")
    print ("Kondiguration anzeigen:")
    print ("  hmgcli config_example show\n\n")
    sys.exit()

def config_example_httpcheck():
    print ("Start httpcheck")
    config_read()
    print ("URL:", cfg["url"] )

def config_read():
    global url, user, password
    from configparser import SafeConfigParser

    parser = SafeConfigParser()
    print ("Read config file: ", configfile)
    my_file = Path(os.path.expanduser(configfile))
    if my_file.is_file():
        parser.read(os.path.expanduser(configfile))
        for name, value in parser.items(module_name):
            cfg[name] = value
        print
    else:
        print ("Configfile existiert nicht.")
        print ("Config erstellen mit: 'hmgcli dns config'")
        sys.exit()

def config_example_show():
    global dnskey, dnsserver
    from configparser import SafeConfigParser

    parser = SafeConfigParser()
    print ("Read config from: ", configfile)
    my_file = Path(os.path.expanduser(configfile))
    if my_file.is_file():
        parser.read(os.path.expanduser(configfile))
        for section_name in parser.sections():
          print ('Section:', section_name )
          print ('  Options:', parser.options(section_name) )
          for name, value in parser.items(section_name):
            print ('  %s = %s' % (name, value) )
            print
    else:
        print ("Configfile existiert nicht.")
        print ("Config create with: 'hmgcli " + module_name + " config create'")
        sys.exit()

def config_example_create():
    import configparser
    config = configparser.ConfigParser()
    config.add_section(module_name)
    for key in configkeys:
      txt = input(key + ": ")
      config[module_name][key] = txt
    print ("Write config file: ", configfile)
    with open(os.path.expanduser(configfile), 'w') as f:
        config.write(f)

# try: command
# except NameError: command = None
# if command is None:
#    config_example_help()
